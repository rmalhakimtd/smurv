import Vue from "vue";
import App from "./App.vue";
import BootstrapVue from "bootstrap-vue";
import VueApexCharts from "vue-apexcharts";
import Vuelidate from "vuelidate";
import VueSweetalert2 from "vue-sweetalert2";
import VueMask from "v-mask";
import * as VueGoogleMaps from "vue2-google-maps";
import VueYoutube from "vue-youtube";

import vco from "v-click-outside";

import router from "./router";
import store from "@/state/store";
import i18n from "./i18n";

import "@/assets/scss/app.scss";
// import firebaseMessaging from './firebase';
import "firebase/messaging";

// Vue.prototype.$messaging = firebaseMessaging;

import firebase from "firebase/app";
import "firebase/firebase-messaging";

firebase.initializeApp({
  apiKey: "AIzaSyCAWdv-pqQkQJBo_sn99T0hFZBb781MGFI",
  authDomain: "smurv-apps.firebaseapp.com",
  projectId: "smurv-apps",
  storageBucket: "smurv-apps.appspot.com",
  messagingSenderId: "450764733091",
  appId: "1:450764733091:web:d4c69d424bb60f295b1d16",
  measurementId: "G-EVLCPC3LTD",
});

// navigator.serviceWorker
//   .register('firebase-messaging-sw.js')
//   .then((registration) => {
//     const messaging = firebase.messaging();
//     messaging.useServiceWorker(registration);
//   })
//   .catch((err) => {
//     console.log(err);
//   });

Vue.config.productionTip = false;
Vue.use(VueYoutube);
Vue.use(BootstrapVue);
Vue.use(vco);
Vue.use(Vuelidate);
Vue.use(VueSweetalert2);
Vue.use(VueMask);
Vue.use(require("vue-chartist"));
Vue.use(VueGoogleMaps, {
  load: {
    key: "AIzaSyAbvyBxmMbFhrzP9Z8moyYr6dCr-pzjhBE",
    libraries: "places",
  },
  installComponents: true,
});
Vue.component("apexchart", VueApexCharts);

new Vue({
  router,
  store,
  i18n,
  render: (h) => h(App),
}).$mount("#app");
