export const menuItems = [
  {
    id: 1,
    label: 'Menu',
    isTitle: true,
    permission: null,
  },
  {
    id: 2,
    label: 'Dashboard',
    icon: 'ri-dashboard-line',
    link: '/',
    permission: 'dashboard',
  },
  {
    id: 3,
    label: 'Alert Summary',
    icon: 'ri-alarm-warning-line',
    link: '/alert-summary',
  },
  {
    id: 4,
    label: 'Inbox',
    isTitle: true,
  },
  {
    id: 5,
    label: 'Exclude Request',
    icon: 'ri-question-line',
    link: '/exclude-request',
    badge: {
      variant: 'danger',
      text: '199',
    },
  },
  {
    id: 11,
    label: 'Dismantle Validation',
    icon: 'ri-checkbox-circle-line',
    link: '/dismantle-validation',
    badge: {
      variant: 'danger',
      text: '199',
    },
  },
  {
    id: 6,
    label: 'Master Data',
    isTitle: true,
  },
  {
    id: 7,
    label: 'Table Exclude',
    icon: 'ri-table-line',
    link: '/tabel-exclude',
  },
  {
    id: 12,
    label: 'Table Dismantle',
    icon: 'ri-table-line',
    link: '/tabel-dismantle',
  },
  {
    id: 8,
    label: 'Master Data',
    isTitle: true,
  },
  {
    id: 9,
    label: 'Role Mangement',
    icon: 'ri-user-settings-line',
    link: '/role-management/roles',
  },
  {
    id: 10,
    label: 'User Management',
    icon: 'ri-group-line',
    link: '/user-management',
  },
  // {
  //   id: 11,
  //   label: 'Menu Management',
  //   icon: 'ri-menu-4-line',
  //   link: '/menu-management',
  // },
];
