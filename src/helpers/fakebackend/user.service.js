import { authHeader } from './auth-header';

export const userService = {
  login,
  logout,
  addUser,
  getAll,
  deleteUser,
  updateUser,
  getCurrentUser,
  getUserMenu,
  addOrganization,
};

const BASE_URL = process.env.VUE_APP_BASEURL;

function login(username, password) {
  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ username, password }),
  };

  return fetch(`${BASE_URL}/auth`, requestOptions)
    .then(handleResponse)
    .then((user) => {
      // login successful if there's a jwt token in the response
      if (user.token) {
        // store user details and jwt token in local storage to keep user logged in between page refreshes
        localStorage.setItem('user', JSON.stringify(user));
      }
      return user;
    });
}

function logout() {
  // remove user from local storage to log user out
  localStorage.removeItem('user');
}

function getAll(data) {
  const requestOptions = {
    method: 'GET',
    headers: authHeader(),
  };
  let url = new URL(`${BASE_URL}/user`),
    params = data;
  if (params != null)
    Object.keys(params).forEach((key) =>
      url.searchParams.append(key, params[key]),
    );
  return fetch(url, requestOptions).then(handleResponse);
}

function getCurrentUser() {
  const requestOptions = {
    method: 'GET',
    headers: authHeader(),
  };
  return fetch(`${BASE_URL}/auth`, requestOptions).then(handleResponse);
}

function getUserMenu() {
  const requestOptions = {
    method: 'GET',
    headers: authHeader(),
  };
  return fetch(`${BASE_URL}/user/permission`, requestOptions).then(
    handleResponse,
  );
}

function addUser(user) {
  const requestOptions = {
    method: 'POST',
    headers: authHeader(),
    body: JSON.stringify(user),
  };
  return fetch(`${BASE_URL}/user`, requestOptions).then(handleResponse);
}
function addOrganization(data) {
  console.log(data);
  const requestOptions = {
    method: 'POST',
    headers: authHeader(),
    body: JSON.stringify({ id: data.organization_id }),
  };
  return fetch(`${BASE_URL}/user/${data.id}/organization`, requestOptions).then(
    handleResponse,
  );
}

function deleteUser(id) {
  const requestOptions = {
    method: 'DELETE',
    headers: authHeader(),
  };
  return fetch(`${BASE_URL}/user/${id}`, requestOptions).then(handleResponse);
}

function updateUser(user) {
  const requestOptions = {
    method: 'PATCH',
    headers: authHeader(),
    body: JSON.stringify(user),
  };
  return fetch(`${BASE_URL}/user/${user.id}`, requestOptions).then(
    handleResponse,
  );
}

function handleResponse(response) {
  return response.text().then((text) => {
    const data = text && JSON.parse(text);
    if (!response.ok) {
      const error = (data && data.message) || response.statusText;
      if (
        response.status === 401 ||
        error === 'jwt malformed' ||
        error === 'jwt expired'
      ) {
        // auto logout if 401 response returned from api
        logout();
        location.reload(true);
      }
      return Promise.reject(error);
    }
    return data;
  });
}
