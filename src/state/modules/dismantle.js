import { dismantleService } from '../../helpers/fakebackend/dismantle.service';

export const state = {
  dismantle: null,
  info: null,
  total: 0,
  isLoading: false,
};

export const actions = {
  // Get All Roles
  getAllDismantle({ dispatch, commit }, data) {
    dismantleService.getAllDismantle(data).then(
      (dismantle) => {
        commit('getAllSuccess', dismantle);
      },
      (error) => {
        dispatch('notification/error', error, { root: true });
      },
    );
  },
  getAllDismantleTotal({ dispatch, commit }, data) {
    dismantleService.getAllDismantle(data).then(
      (dismantle) => {
        commit('getTotal', dismantle);
      },
      (error) => {
        dispatch('notification/error', error, { root: true });
      },
    );
  },
  getDismantleInfo({ dispatch, commit }, id) {
    dismantleService.getDismantleInfo(id).then(
      (dismantle) => {
        commit('getInfoSuccess', dismantle);
      },
      (error) => {
        dispatch('notification/error', error, { root: true });
      },
    );
  },
};

export const mutations = {
  getAllSuccess(state, dismantle) {
    state.dismantle = dismantle;
  },
  getTotal(state, dismantle) {
    state.total = dismantle.data.length;
    // console.log(dismantle.data.length);
  },
  getInfoSuccess(state, dismantle) {
    state.info = dismantle;
  },
  // pushRoleToState(state, role) {
  //   state.roles.push(role);
  // },
  // removeFromState(state, id) {
  //   state.roles = state.roles.filter((e) => {
  //     return e.id != id;
  //   });
  // },
  // updateState(state, role) {
  //   let index = state.roles.findIndex((obj) => obj.id === role.id);

  //   state.roles[index].role_name = role.role_name;
  // },
  isLoadingTrue(state) {
    state.isLoading = true;
  },
  isLoadingFalse(state) {
    state.isLoading = false;
  },
};
