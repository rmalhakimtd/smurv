import { menuService } from '../../helpers/fakebackend/menu.service';

export const state = {
  menu: null,
  isLoading: false,
};

export const actions = {
  getAllMenu({ dispatch, commit }) {
    menuService.getAll().then(
      (menus) => {
        commit('getAllSuccess', menus);
      },
      (error) => {
        dispatch('notification/error', error, { root: true });
      },
    );
  },
  addMenu({ dispatch, commit }, menu) {
    commit('isLoadingTrue');
    menuService.addMenu(menu).then(
      (menu) => {
        commit('isLoadingFalse');
        commit('pushMenuToState', menu);
        dispatch('notification/success', 'Berhasil menambah menu', {
          root: true,
        });
      },
      (error) => {
        commit('isLoadingFalse');
        dispatch('notification/error', error, { root: true });
      },
    );
  },
  // eslint-disable-next-line no-unused-vars
  deleteMenuById({ dispatch, commit }, item) {
    commit('isLoadingTrue');
    menuService.deleteMenu(item.id).then(
      () => {
        commit('isLoadingFalse');
        commit('removeFromState', item);
        dispatch('notification/success', 'Berhasil menghapus menu', {
          root: true,
        });
      },
      (error) => {
        commit('isLoadingFalse');
        dispatch('notification/error', error, { root: true });
      },
    );
  },
};

export const mutations = {
  getAllSuccess(state, menus) {
    console.log(menus.data.data);
    state.menu = menus.data.data.map((e) => ({
      id: e.id,
      text: e.menu_name,
      icon: e.icon,
      opened: true,
      children: e.children.map((val) => ({
        id: val.id,
        text: val.menu_name,
        icon: val.icon,
        value: e,
      })),
      value: e,
    }));
  },
  pushMenuToState(state, menu) {
    if (menu.parent_id !== 0) {
      let index = state.menu.findIndex((obj) => obj.id === menu.parent_id);
      state.menu[index].children.push({
        id: menu.id,
        text: menu.menu_name,
        icon: menu.icon,
        opened: true,
        value: menu,
      });
    } else {
      state.menu.push({
        id: menu.id,
        text: menu.menu_name,
        icon: menu.icon,
        opened: true,
        value: menu,
        children: [],
      });
    }
  },

  removeFromState(_, item) {
    item.node.parentItem.splice(item.index, 1);
  },
  isLoadingTrue(state) {
    state.isLoading = true;
  },
  isLoadingFalse(state) {
    state.isLoading = false;
  },
};
