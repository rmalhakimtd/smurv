import { excludeService } from '../../helpers/fakebackend/exclude.service';

export const state = {
  exclude: null,
  isLoading: false,
  total: 0,
};

export const actions = {
  // Get All Roles
  getAllExclude({ dispatch, commit }, data) {
    excludeService.getAllExclude(data).then(
      (exclude) => {
        commit('getAllSuccess', exclude);
      },
      (error) => {
        dispatch('notification/error', error, { root: true });
      },
    );
  },
  getAllExcludeTotal({ dispatch, commit }, data) {
    excludeService.getAllExclude(data).then(
      (exclude) => {
        commit('getTotal', exclude);
      },
      (error) => {
        dispatch('notification/error', error, { root: true });
      },
    );
  },
};

export const mutations = {
  getAllSuccess(state, exclude) {
    state.exclude = exclude;
  },
  getTotal(state, exclude) {
    state.total = exclude.total;
  },
  // pushRoleToState(state, role) {
  //   state.roles.push(role);
  // },
  // removeFromState(state, id) {
  //   state.roles = state.roles.filter((e) => {
  //     return e.id != id;
  //   });
  // },
  // updateState(state, role) {
  //   let index = state.roles.findIndex((obj) => obj.id === role.id);

  //   state.roles[index].role_name = role.role_name;
  // },
  isLoadingTrue(state) {
    state.isLoading = true;
  },
  isLoadingFalse(state) {
    state.isLoading = false;
  },
};
