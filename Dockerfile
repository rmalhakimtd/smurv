FROM node:12-alpine as build

WORKDIR /app

ARG VUE_APP_API_HOST

COPY package.json .
RUN yarn

COPY . .

RUN yarn build

FROM nginx:alpine

WORKDIR /usr/share/nginx/html

ENV VUE_APP_API_HOST=https://smurv.telkom.co.id/api

RUN rm -rf ./*

COPY --from=build /app/dist .
COPY nginx.conf /etc/nginx/nginx.conf
COPY entrypoint.sh .

RUN chmod +x ./entrypoint.sh

ENTRYPOINT ["./entrypoint.sh" ]