import { authHeader } from './auth-header';

export const deviceService = { getAll, addDevice, updateDevice, deleteDevice };

const BASE_URL = process.env.VUE_APP_BASEURL;

function getAll() {
  const requestOptions = {
    method: 'GET',
    headers: authHeader(),
  };
  return fetch(`${BASE_URL}/perangkat`, requestOptions).then(handleResponse);
}

function addDevice(data) {
  const requestOptions = {
    method: 'POST',
    headers: authHeader(),
    body: JSON.stringify(data),
  };
  return fetch(`${BASE_URL}/perangkat`, requestOptions).then(handleResponse);
}

function updateDevice(data) {
  const requestOptions = {
    method: 'PATCH',
    headers: authHeader(),
    body: JSON.stringify(data),
  };
  return fetch(`${BASE_URL}/perangkat/${data.id}`, requestOptions).then(
    handleResponse,
  );
}

function deleteDevice(id) {
  const requestOptions = {
    method: 'DELETE',
    headers: authHeader(),
  };
  return fetch(`${BASE_URL}/perangkat/${id}`, requestOptions).then(
    handleResponse,
  );
}

function handleResponse(response) {
  return response.text().then((text) => {
    const data = text && JSON.parse(text);
    if (!response.ok) {
      const error = (data && data.message) || response.statusText;
      if (
        response.status === 401 ||
        error === 'jwt malformed' ||
        error === 'jwt expired'
      ) {
        // auto logout if 401 response returned from api
        localStorage.removeItem('user');
        location.reload(true);
      }
      return Promise.reject(error);
    }
    return data;
  });
}
