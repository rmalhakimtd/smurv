import { authHeader } from './auth-header';

export const menuService = {
  getAll,
  addMenu,
  deleteMenu,
};

const BASE_URL = process.env.VUE_APP_BASEURL;

function getAll() {
  const requestOptions = {
    method: 'GET',
    headers: authHeader(),
  };
  return fetch(`${BASE_URL}/menu`, requestOptions).then(handleResponse);
}

function addMenu(menu) {
  const requestOptions = {
    method: 'POST',
    headers: authHeader(),
    body: JSON.stringify(menu),
  };
  return fetch(`${BASE_URL}/menu`, requestOptions).then(handleResponse);
}

function deleteMenu(id) {
  const requestOptions = {
    method: 'DELETE',
    headers: authHeader(),
  };
  return fetch(`${BASE_URL}/menu/${id}`, requestOptions).then(handleResponse);
}

function handleResponse(response) {
  return response.text().then((text) => {
    const data = text && JSON.parse(text);
    if (!response.ok) {
      const error = (data && data.message) || response.statusText;
      if (
        response.status === 401 ||
        error === 'jwt malformed' ||
        error === 'jwt expired'
      ) {
        // auto logout if 401 response returned from api
        localStorage.removeItem('user');
        location.reload(true);
      }
      return Promise.reject(error);
    }
    return data;
  });
}
