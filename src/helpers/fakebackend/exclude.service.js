import { authHeader } from './auth-header';
export const excludeService = {
  getAllExclude,
};

const BASE_URL = process.env.VUE_APP_BASEURL;

function getAllExclude(data) {
  const requestOptions = {
    method: 'GET',
    headers: authHeader(),
  };
  let url = new URL(`${BASE_URL}/exclude`),
    params = data;
  if (params != null)
    Object.keys(params).forEach((key) =>
      url.searchParams.append(key, params[key]),
    );
  return fetch(url, requestOptions).then(handleResponse);
}
// function getExcludeInfo(id) {
//   const requestOptions = {
//     method: "GET",
//     headers: authHeader(),
//   };
//   return fetch(`${BASE_URL}/dismantle/response/${id}`, requestOptions).then(
//     handleResponse
//   );
// }

function handleResponse(response) {
  return response.text().then((text) => {
    const data = text && JSON.parse(text);
    if (!response.ok) {
      const error = (data && data.message) || response.statusText;
      if (
        response.status === 401 ||
        error === 'jwt malformed' ||
        error === 'jwt expired'
      ) {
        // auto logout if 401 response returned from api
        localStorage.removeItem('user');
        location.reload(true);
      }
      return Promise.reject(error);
    }
    return data;
  });
}
