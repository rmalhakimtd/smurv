import { authHeader } from './auth-header';
export const dismantleService = {
  getAllDismantle,
  getDismantleInfo,
};

const BASE_URL = process.env.VUE_APP_BASEURL;

function getAllDismantle(data) {
  const requestOptions = {
    method: 'GET',
    headers: authHeader(),
  };
  let url = new URL(`${BASE_URL}/dismantle`),
    params = data;
  if (params != null)
    Object.keys(params).forEach((key) => {
      // if (key !== 'start_date' && key !== 'end_date') {
      url.searchParams.append(key, params[key]);
      // }
    });

  let decodeColon = url.toString().replaceAll('%3A', ':');

  let finalUrl = decodeColon.replaceAll('%2B', '+');
  return fetch(finalUrl, requestOptions).then(handleResponse);
}
function getDismantleInfo(id) {
  const requestOptions = {
    method: 'GET',
    headers: authHeader(),
  };
  return fetch(`${BASE_URL}/dismantle/response/${id}`, requestOptions).then(
    handleResponse,
  );
}

function handleResponse(response) {
  return response.text().then((text) => {
    const data = text && JSON.parse(text);
    if (!response.ok) {
      const error = (data && data.message) || response.statusText;
      if (
        response.status === 401 ||
        error === 'jwt malformed' ||
        error === 'jwt expired'
      ) {
        // auto logout if 401 response returned from api
        localStorage.removeItem('user');
        location.reload(true);
      }
      return Promise.reject(error);
    }
    return data;
  });
}
