import firebase from "firebase/app";
import "firebase/firebase-messaging";

const firebaseConfig = {
  apiKey: "AIzaSyCAWdv-pqQkQJBo_sn99T0hFZBb781MGFI",
  authDomain: "smurv-apps.firebaseapp.com",
  projectId: "smurv-apps",
  storageBucket: "smurv-apps.appspot.com",
  messagingSenderId: "450764733091",
  appId: "1:450764733091:web:d4c69d424bb60f295b1d16",
  measurementId: "G-EVLCPC3LTD",
};

firebase.initializeApp(firebaseConfig);

export default firebase.messaging();
