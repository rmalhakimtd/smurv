import { deviceService } from '../../helpers/fakebackend/device.service';

export const state = {
  device: null,
  isLoading: false,
};

export const actions = {
  getAllDevice({ dispatch, commit }) {
    deviceService.getAll().then(
      (data) => {
        commit('getAllSuccess', data);
      },
      (error) => {
        dispatch('notification/error', error, { root: true });
      },
    );
  },
  addDevice({ dispatch, commit }, data) {
    commit('isLoadingTrue');
    deviceService.addDevice(data).then(
      (perangkat) => {
        commit('isLoadingFalse');
        commit('pushRoleToState', perangkat);
        dispatch('notification/success', 'Berhasil menambah perangkat', {
          root: true,
        });
      },
      (error) => {
        commit('isLoadingFalse');
        dispatch('notification/error', error, { root: true });
      },
    );
  },
  deleteDeviceById({ dispatch, commit }, id) {
    commit('isLoadingTrue');
    deviceService.deleteDevice(id).then(
      () => {
        commit('isLoadingFalse');
        commit('removeFromState', id);
        dispatch('notification/success', 'Berhasil menghapus perangkat', {
          root: true,
        });
      },
      (error) => {
        commit('isLoadingFalse');
        dispatch('notification/error', error, { root: true });
      },
    );
  },
  updateDeviceById({ dispatch, commit }, data) {
    commit('isLoadingTrue');
    deviceService.updateDevice(data).then(
      () => {
        commit('isLoadingFalse');
        dispatch('getAllDevice');
        dispatch('notification/success', 'Berhasil mengubah perangkat', {
          root: true,
        });
      },
      (error) => {
        commit('isLoadingFalse');
        dispatch('notification/error', error, { root: true });
      },
    );
  },
};

export const mutations = {
  getAllSuccess(state, device) {
    state.device = device;
  },
  pushRoleToState(state, data) {
    state.device.data.push(data.data);
  },
  removeFromState(state, id) {
    state.device.data = state.device.data.filter((e) => {
      return e.id != id;
    });
  },
  updateState(state, data) {
    let index = state.device.data.findIndex((obj) => obj.id === data.id);

    state.device.data[index].device_name = data.device_name;
    state.device.data[index].prefix = data.prefix;
    state.device.data[index].detail = data.detail;
    state.device.data[index].owner = data.owner;
    state.device.data[index].organization = data.organization;
    state.device.data[index].id = data.id;
    state.device.data[index].createdAt = data.createdAt;
    state.device.data[index].updatedAt = data.updatedAt;
  },
  isLoadingTrue(state) {
    state.isLoading = true;
  },
  isLoadingFalse(state) {
    state.isLoading = false;
  },
};
