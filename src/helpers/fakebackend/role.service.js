import { authHeader } from './auth-header';
export const roleService = {
  addRole,
  getAll,
  deleteRole,
  updateRole,
};

const BASE_URL = process.env.VUE_APP_BASEURL;

function addRole(role_name) {
  const requestOptions = {
    method: 'POST',
    headers: authHeader(),
    body: JSON.stringify(role_name),
  };
  return fetch(`${BASE_URL}/role`, requestOptions).then(handleResponse);
}

function deleteRole(id) {
  const requestOptions = {
    method: 'DELETE',
    headers: authHeader(),
  };
  return fetch(`${BASE_URL}/role/${id}`, requestOptions).then(handleResponse);
}

function updateRole(role) {
  const requestOptions = {
    method: 'PATCH',
    headers: authHeader(),
    body: JSON.stringify(role),
  };
  return fetch(`${BASE_URL}/role/${role.id}`, requestOptions).then(
    handleResponse,
  );
}

function getAll() {
  const requestOptions = {
    method: 'GET',
    headers: authHeader(),
  };
  return fetch(`${BASE_URL}/role`, requestOptions).then(handleResponse);
}

function handleResponse(response) {
  return response.text().then((text) => {
    const data = text && JSON.parse(text);
    if (!response.ok) {
      const error = (data && data.message) || response.statusText;
      if (
        response.status === 401 ||
        error === 'jwt malformed' ||
        error === 'jwt expired'
      ) {
        // auto logout if 401 response returned from api
        localStorage.removeItem('user');
        location.reload(true);
      }
      return Promise.reject(error);
    }
    return data;
  });
}
