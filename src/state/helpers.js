import { mapState, mapGetters, mapActions } from 'vuex';

export const authComputed = {
  ...mapState('auth', {
    currentUser: (state) => state.currentUser,
  }),
  ...mapGetters('auth', ['loggedIn']),
};

export const layoutComputed = {
  ...mapState('layout', {
    layoutType: (state) => state.layoutType,
    leftSidebarType: (state) => state.leftSidebarType,
    layoutWidth: (state) => state.layoutWidth,
    topbar: (state) => state.topbar,
    loader: (state) => state.loader,
  }),
};

export const authMethods = mapActions('auth', [
  'logIn',
  'logOut',
  'register',
  'resetPassword',
]);

export const layoutMethods = mapActions('layout', [
  'changeLayoutType',
  'changeLayoutWidth',
  'changeLeftSidebarType',
  'changeTopbar',
  'changeLoaderValue',
]);

export const authFackMethods = mapActions('authfack', [
  'login',
  'registeruser',
  'logout',
  'getCurrentUser',
  'updateUserById',
  'getUserMenu',
]);

export const notificationMethods = mapActions('notification', [
  'success',
  'error',
  'clear',
]);

export const roleMethods = mapActions('role', [
  'getAll',
  'addRole',
  'deleteRoleById',
  'updateRoleById',
]);

export const deviceMethods = mapActions('device', [
  'getAllDevice',
  'addDevice',
  'deleteDeviceById',
  'updateDeviceById',
]);

export const userMethods = mapActions('user', [
  'getAllUser',
  'addUser',
  'deleteUserById',
  'updateUserById',
]);

export const menuMethods = mapActions('menu', [
  'getAllMenu',
  'addMenu',
  'deleteMenuById',
]);

export const alarmMethods = mapActions('alarm', ['getSummaryData']);
export const permissionMethods = mapActions('permission', [
  'getAllPermissionOptions',
]);

export const organizationMethods = mapActions('organization', [
  'getOrganiztionOptions',
]);

export const dismantleMethods = mapActions('dismantle', [
  'getAllDismantle',
  'getDismantleInfo',
  'getAllDismantleTotal',
]);

export const excludeMethods = mapActions('exclude', [
  'getAllExclude',
  'getAllExcludeTotal',
]);

export const notifikasiMethods = mapActions('notifikasi', [
  'getAllNotifications',
]);
