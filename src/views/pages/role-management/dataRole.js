const tableData = [
  {
    id: 1,
    name: 'SM - GA',
  },
  {
    id: 2,
    name: 'Superadmin',
  },
  {
    id: 3,
    name: 'GA - Staff',
  },
  {
    id: 4,
    name: 'GA - Manager',
  },
  {
    id: 5,
    name: 'User DSO',
  },
];

export { tableData };
