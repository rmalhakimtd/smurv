#!/bin/sh
echo "Replacing env constants in JS"
sed -i 's|<VUE_APP_API_HOST>|'${VUE_APP_API_HOST}'|g' ./js/app.*.js*;

echo "Running Server"
nginx -g "daemon off;";