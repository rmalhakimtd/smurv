import { userService } from '../../helpers/fakebackend/user.service';

export const state = {
  users: null,
  isLoading: false,
};

export const actions = {
  // Get All Roles
  getAllUser({ dispatch, commit }, data) {
    userService.getAll(data).then(
      (users) => {
        commit('getAllSuccess', users);
      },
      (error) => {
        dispatch('notification/error', error, { root: true });
      },
    );
  },
  addUser({ dispatch, commit }, user) {
    commit('isLoadingTrue');
    userService.addUser(user).then(
      (user) => {
        commit('isLoadingFalse');
        commit('pushUserToState', user);

        dispatch('notification/success', 'Berhasil menambah user', {
          root: true,
        });
      },
      (error) => {
        commit('isLoadingFalse');
        dispatch('notification/error', error, { root: true });
      },
    );
  },
  addOrganization({ dispatch, commit }, user) {
    userService.addOrganization(user).then(
      (user) => {
        commit('isLoadingFalse');
        commit('updateState', user);
      },
      (error) => {
        dispatch('notification/error', error, { root: true });
      },
    );
  },
  deleteUserById({ dispatch, commit }, id) {
    commit('isLoadingTrue');
    userService.deleteUser(id).then(
      () => {
        commit('isLoadingFalse');
        commit('removeFromState', id);
        dispatch('notification/success', 'Berhasil menghapus user', {
          root: true,
        });
      },
      (error) => {
        commit('isLoadingFalse');
        dispatch('notification/error', error, { root: true });
      },
    );
  },
  updateUserById({ dispatch, commit }, user) {
    commit('isLoadingTrue');
    userService.updateUser(user).then(
      (user) => {
        commit('isLoadingFalse');
        commit('updateState', user);

        dispatch('notification/success', 'Berhasil mengubah user', {
          root: true,
        });
      },
      (error) => {
        commit('isLoadingFalse');
        dispatch('notification/error', error, { root: true });
      },
    );
  },
};

export const mutations = {
  getAllSuccess(state, users) {
    console.log(users);
    state.users = users;
  },
  pushUserToState(state, user) {
    state.users.push(user.data);
  },
  removeFromState(state, id) {
    state.users = state.users.filter((e) => {
      return e.id != id;
    });
  },
  updateState(state, user) {
    let index = state.users.findIndex((obj) => obj.id === user.id);

    state.users[index].username = user.username;
    state.users[index].nama = user.nama;
    state.users[index].password = user.password;
    state.users[index].ldapflag = user.ldapflag;
    state.users[index].activeflag = user.activeflag;
    state.users[index].registration_id = user.registration_id;
    state.users[index].jenis_user = user.jenis_user;
    state.users[index].email = user.email;
    state.users[index].no_hp = user.no_hp;
    state.users[index].ktp = user.ktp;
    state.users[index].exp_date = user.exp_date;
    state.users[index].roles = user.roles;
  },
  isLoadingTrue(state) {
    state.isLoading = true;
  },
  isLoadingFalse(state) {
    state.isLoading = false;
  },
};
