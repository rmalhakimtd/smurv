import { userService } from '../../helpers/fakebackend/user.service';
import router from '../../router/index';

const user = JSON.parse(localStorage.getItem('user'));
export const state = user
  ? { status: { loggeduser: true }, user, currentUser: {}, menu: {} }
  : { status: {}, user: null, currentUser: {}, menu: {} };

export const actions = {
  // Logs in the user.
  // eslint-disable-next-line no-unused-vars
  login({ dispatch, commit }, { username, password }) {
    commit('loginRequest', { username });

    userService.login(username, password).then(
      (user) => {
        commit('loginSuccess', user);
        router.push('/');
      },
      (error) => {
        commit('loginFailure', error);
        dispatch('notification/error', error, { root: true });
      },
    );
  },
  getCurrentUser({ dispatch, commit }) {
    userService.getCurrentUser().then(
      (user) => {
        commit('getSuccess', user);
      },
      (error) => {
        dispatch('notification/error', error, { root: true });
      },
    );
  },
  getUserMenu({ dispatch, commit }) {
    userService.getUserMenu().then(
      (menu) => {
        commit('getMenuSuccess', menu);
      },
      (error) => {
        dispatch('notification/error', error, { root: true });
      },
    );
  },
  // Logout the user
  logout({ commit }) {
    userService.logout();
    commit('logout');
  },
  // register the user
  registeruser({ dispatch, commit }, user) {
    commit('registerRequest', user);
    userService.register(user).then(
      (user) => {
        commit('registerSuccess', user);
        dispatch('notification/success', 'Registration successful', {
          root: true,
        });
        router.push('/login');
      },
      (error) => {
        commit('registerFailure', error);
        dispatch('notification/error', error, { root: true });
      },
    );
  },
  updateUserById({ dispatch, commit }, user) {
    // commit('isLoadingTrue');
    userService.updateUser(user).then(
      (user) => {
        // commit('isLoadingFalse');
        commit('getSuccess', user);
        dispatch('notification/success', 'Berhasil mengubah user', {
          root: true,
        });
      },
      (error) => {
        // commit('isLoadingFalse');
        dispatch('notification/error', error, { root: true });
      },
    );
  },
};

export const mutations = {
  getSuccess(state, user) {
    state.currentUser = user;
  },
  getMenuSuccess(state, menu) {
    state.menu = menu;
  },
  loginRequest(state, user) {
    state.status = { loggingIn: true };
    state.user = user;
  },
  loginSuccess(state, user) {
    state.status = { loggeduser: true };
    state.user = user;
  },
  loginFailure(state) {
    state.status = {};
    state.user = null;
  },
  logout(state) {
    state.status = {};
    state.user = null;
  },
  registerRequest(state) {
    state.status = { registering: true };
  },
  registerSuccess(state) {
    state.status = {};
  },
  registerFailure(state) {
    state.status = {};
  },
};
