import { notificationService } from '../../helpers/fakebackend/notification.service';

export const state = {
  notifikasi: null,
  isLoading: false,
};

export const actions = {
  // Get All Roles
  getAllNotifications({ dispatch, commit }) {
    notificationService.getAllNotifications().then(
      (notif) => {
        commit('getAllSuccess', notif);
      },
      (error) => {
        dispatch('notification/error', error, { root: true });
      },
    );
  },
  // addRole({ dispatch, commit }, role_name) {
  //   commit('isLoadingTrue');
  //   roleService.addRole(role_name).then(
  //     (role) => {
  //       commit('isLoadingFalse');
  //       commit('pushRoleToState', role);
  //       dispatch('notification/success', 'Berhasil menambah role', {
  //         root: true,
  //       });
  //     },
  //     (error) => {
  //       commit('isLoadingFalse');
  //       dispatch('notification/error', error, { root: true });
  //     },
  //   );
  // },
};

export const mutations = {
  getAllSuccess(state, notif) {
    state.notifikasi = notif;
  },
  // pushRoleToState(state, role) {
  //   state.roles.data.push(role);
  // },
  // removeFromState(state, id) {
  //   state.roles.data = state.roles.data.filter((e) => {
  //     return e.id != id;
  //   });
  // },
  // updateState(state, role) {
  //   let index = state.roles.data.findIndex((obj) => obj.id === role.id);

  //   state.roles.data[index].role_name = role.role_name;
  //   state.roles.data[index].permissions = role.permissions;
  // },
  isLoadingTrue(state) {
    state.isLoading = true;
  },
  isLoadingFalse(state) {
    state.isLoading = false;
  },
};
