import { alarmService } from '../../helpers/api/alarm.service';

export const state = {
  alarms: null,
  isLoading: false,
};

export const actions = {
  // Get All Roles
  getSummaryData({ dispatch, commit }, data) {
    commit('isLoadingTrue');
    alarmService.getAllSummary(data).then(
      (role) => {
        commit('isLoadingFalse');
        commit('getAllSuccess', role);
      },
      (error) => {
        commit('isLoadingFalse');
        dispatch('notification/error', error, { root: true });
      },
    );
  },
};

export const mutations = {
  getAllSuccess(state, alarms) {
    state.alarms = alarms;
  },
  isLoadingTrue(state) {
    state.isLoading = true;
  },
  isLoadingFalse(state) {
    state.isLoading = false;
  },
};
