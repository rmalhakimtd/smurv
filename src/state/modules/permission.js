import { permissionService } from '../../helpers/fakebackend/permission.service';

export const state = {
  options: null,
  isLoading: false,
};

export const actions = {
  // Get All Roles
  getAllPermissionOptions({ dispatch, commit }) {
    permissionService.getAllOptions().then(
      (options) => {
        commit('getAllOptionsSuccess', options);
      },
      (error) => {
        dispatch('notification/error', error, { root: true });
      },
    );
  },
};

export const mutations = {
  getAllOptionsSuccess(state, options) {
    state.options = options;
  },
  // pushRoleToState(state, role) {
  //   state.roles.push(role);
  // },
  // removeFromState(state, id) {
  //   state.roles = state.roles.filter((e) => {
  //     return e.id != id;
  //   });
  // },
  // updateState(state, role) {
  //   let index = state.roles.findIndex((obj) => obj.id === role.id);

  //   state.roles[index].role_name = role.role_name;
  // },
  isLoadingTrue(state) {
    state.isLoading = true;
  },
  isLoadingFalse(state) {
    state.isLoading = false;
  },
};
