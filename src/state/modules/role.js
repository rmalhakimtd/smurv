import { roleService } from '../../helpers/fakebackend/role.service';

export const state = {
  roles: null,
  isLoading: false,
};

export const actions = {
  // Get All Roles
  getAll({ dispatch, commit }) {
    roleService.getAll().then(
      (role) => {
        commit('getAllSuccess', role);
      },
      (error) => {
        dispatch('notification/error', error, { root: true });
      },
    );
  },
  addRole({ dispatch, commit }, role_name) {
    commit('isLoadingTrue');
    roleService.addRole(role_name).then(
      (role) => {
        commit('isLoadingFalse');
        commit('pushRoleToState', role);
        dispatch('notification/success', 'Berhasil menambah role', {
          root: true,
        });
      },
      (error) => {
        commit('isLoadingFalse');
        dispatch('notification/error', error, { root: true });
      },
    );
  },
  deleteRoleById({ dispatch, commit }, id) {
    commit('isLoadingTrue');
    roleService.deleteRole(id).then(
      () => {
        commit('isLoadingFalse');
        commit('removeFromState', id);
        dispatch('notification/success', 'Berhasil menghapus role', {
          root: true,
        });
      },
      (error) => {
        commit('isLoadingFalse');
        dispatch('notification/error', error, { root: true });
      },
    );
  },
  updateRoleById({ dispatch, commit }, role) {
    commit('isLoadingTrue');
    roleService.updateRole(role).then(
      (role) => {
        commit('isLoadingFalse');
        commit('updateState', role);
        dispatch('notification/success', 'Berhasil mengubah role', {
          root: true,
        });
      },
      (error) => {
        commit('isLoadingFalse');
        dispatch('notification/error', error, { root: true });
      },
    );
  },
};

export const mutations = {
  getAllSuccess(state, roles) {
    state.roles = roles;
  },
  pushRoleToState(state, role) {
    state.roles.data.push(role);
  },
  removeFromState(state, id) {
    state.roles.data = state.roles.data.filter((e) => {
      return e.id != id;
    });
  },
  updateState(state, role) {
    let index = state.roles.data.findIndex((obj) => obj.id === role.id);

    state.roles.data[index].role_name = role.role_name;
    state.roles.data[index].permissions = role.permissions;
  },
  isLoadingTrue(state) {
    state.isLoading = true;
  },
  isLoadingFalse(state) {
    state.isLoading = false;
  },
};
